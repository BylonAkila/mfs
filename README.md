## Merge FileSystem (fuse)

### Feature:
This fuse filesystem "MergeFS" will virtually merge several files into a single one without having to copy them first as with a `cat` command.

The goal is to save I/O, thus time and wear of disks (especially SSD).

Note that if the only feature needed on the "virtually merged file" is a stream operation, this can be achieved in the standard Linux way with named pipes.

Where this MergeFS comes in handy is when you want to access the "virtually merged file" with a program that does some non-sequential reads like: unrar, vlc, or even mounting an ISO on top of the fuse __mfs__ and doing random access.

__IMPORTANT:__ the mount is read-only by default (same as you get when using named pipes), it is safer!. It can be made read-write by specifiying -orw and will advise to have a backup copy of your files before attempting that!


###  Usage:

Either read the help provided by: `mfs -h` or look at the help section in the code under mfs_opt_count()


###  Typical use case 1 - files:

You have downloaded a BluRay iso from newsgroups as _'split files'_.

_(Obviously it is a BluRay from your uncle showing all the fun he had with his family during the holidays!)_

To be able to mount the iso, you would normally have to start with a `cat` command on the splits to get the merged iso file.

A BluRay could be somewhere between 25GiB and 50GiB, this first `cat` would	probably take several minutes plus use free space on the disk that could be	almost exhausted by the ~40Gb raw split files, and produce wear especially on SSD.

Instead of that, you can just mount the split files with this program, mount the ISO on top of that, and directly play your movie from there!

So, basically, you need no copy at all from the _'split files'_.

That would be impossible with standard mechanism like _"named pipes"_ since	although BluRay films are in the folder "STREAMS" you need some non-sequential reads (seek) on the ISO to be able to play it correctly.


###  Typical use case 2 - partitions:

You suspect you have damaged the MBR of your disk. You have a backup of the MBR and would like to try if it fixes things without incurring further damage.

You could then do something like that:

```sh
sudo mfs /mnt MBR-Backup /dev/sda -t $( stat -c %s MBR-Backup ):
```

This will show the file _MBR-Backup_ in /mnt that is the concatenation of you saved MBR and the rest of the disk.

You can then try with a tool like *losetup* to see if that correctly shows your partitions and if you can mount them. If so you might decide to write the MBR in place to fix things.


###  License:
   
This program is free software: you can redistribute it and/or modify it under the terms of the __GNU General Public License__ as published by the Free Software Foundation, either __version 3__ of the License, or (at your option) any later version.


###  Compile with:
You only need one file to compile: mfs.c

The compile should output no warning or error.

You could then move the _mfs_ program to _/usr/bin_ if you wish to have it comfortably system wide.

```sh
cc -Wall -std=c99 -Wpedantic mfs.c `pkg-config fuse --cflags --libs` -o mfs
```

### Compile dependancy:
You must have _libfuse-dev_ (or equivalent) installed, and standard compile tools (generally	already installed as default in your distribution)

Typically install libfuse-dev with: 

(Debian/Ubuntu) `sudo apt-get install libfuse-dev`

(Fedora)        `sudo dnf install fuse-devel`

... or adapt with the package manager that comes with your distribution.


### TODO
Before requesting a feature see the **TODO** section in _mfs.c_ comments header.
