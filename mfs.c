/*
  mfs: Merge File System

  Copyright (C) 2017-2020  Alain BENEDETTI <alainb06@free.fr>

  License:
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Feature:
	This fuse filesystem "MergeFS" will virtually merge several files into a single
		one without having to copy them first as with a `cat` command.
	The goal is to save I/O, thus time and wear of disks (especially SSD).
	Note that if the only feature needed on the "virtually merged file" is a stream
		operation, this can be achieved in the standard Linux way with named
		pipes.
	Where this MergeFS comes in handy is when you want to access the "virtually
		merged file" with a program that does some non-sequential reads like:
		unrar, vlc, or even mounting an ISO on top of the fuse mfs and
		doing random access.
	IMPORTANT: by default the mount is read-only (safer!). Specify rw to
	           allow writing.

  Usage:
	Either
		read the help provided by: `mfs -h` or
		look at the help section below, under mfs_opt_count()


  Typical use case:
	You have downloaded a BluRay iso as 'split files' from newsgroups.
	(Obviously it is a BluRay from your uncle showing all the fun he had with
	 his family during the holidays!)
	To be able to mount the iso, you would normally have to start with a 'cat'
	command on the splits to get the merged iso file.
	A BluRay could be somewhere between 25GiB and 50GiB, this first 'cat' would
	probably take several minutes plus use free space on the disk that could be
	almost exhausted by the ~40Gb raw split files, and produce wear especially
	on SSD.
	Instead of that, you can just mount the split files with this program,
	mount the ISO on top of that, and directly play your movie from there.
	So, basically, you need no copy at all from the 'split files'.
	That would be impossible with standard mechanism like "named pipes" since
	although BluRay films are in the folder "STREAMS" you need some non-sequential
	read (seek) on the ISO to be able to play it correctly.

  Compile with:
	You only need one file to compile: mfs.c
	The compile should output no warning or error.
	You could then move the mfs program to /usr/bin if you wish to have it
	comfortably system wide.
	
	cc -Wall -std=c99 -Wpedantic mfs.c `pkg-config fuse --cflags --libs` -o mfs

  Compile dependancy:
	You must have libfuse-dev installed, and standard compile tools (generally
	already installed as default in your distribution)
	Typically install libfuse-dev with: 
	(Debian/Ubuntu) sudo apt-get install libfuse-dev
	(Fedora)        sudo dnf install fuse-devel
	... or adapt with the package manager that comes with your distribution.


  Version: 1.2.0

  History:
	2017/08/12: 1.0.0 Initial version
	2018/11/13: 1.1.0 Added support for block devices as chunk files
			  + fixed an issue on total file size of the merge.
			  - Removed binaries
	2020/01/10: 1.2.0 Added support for writing


  TODO:
	We open all the files in the intialisation phase. That could
	be an issue if we really have a lot of files. Then we should have to
	open the files only when needed... with potential errors from deleted files.
	We would also have to have locks, which at the moment we don't need.

	There is also no provision for opening only once a file that was
	several times in the parameters list: mfs mountpoint file1 file1 file1
	This could be useful for generating replicated data without actually
	having to copy it first. Since I don't have that use case so far, it is
	not yet in the code but could be added easily in the initialisation phase
	without infamous locks.
*/

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdarg.h>
#include <syslog.h>
#include <string.h>
#include <linux/fs.h>
#include <sys/ioctl.h>


#define MFS_VERSION "mfs: version 1.0.0\n"

#define MAX_LOG 5

enum {
     KEY_TRANCHE,
     KEY_HELP,
     KEY_VERSION,
     KEY_NO_SOURCE_CACHE,
     KEY_DEBUG,
     KEY_FOREGROUND,
     KEY_RW,
};


static const struct fuse_opt mfs_count[] = {
	FUSE_OPT_KEY( "-t "	 , FUSE_OPT_KEY_DISCARD	),
	FUSE_OPT_KEY( "-h"	 , (int)KEY_HELP	),
	FUSE_OPT_KEY( "--help"	 , (int)KEY_HELP	),
	FUSE_OPT_KEY( "-V"	 , (int)KEY_VERSION	),
	FUSE_OPT_KEY( "--version", (int)KEY_VERSION	),
	FUSE_OPT_KEY( "rw"	 , (int)KEY_RW		),

	FUSE_OPT_KEY( "-d"	 , FUSE_OPT_KEY_DISCARD ),
	FUSE_OPT_KEY( "-f"	 , FUSE_OPT_KEY_DISCARD ),
	FUSE_OPT_KEY( "-o "	 , FUSE_OPT_KEY_DISCARD ),
	FUSE_OPT_KEY( "-s"	 , FUSE_OPT_KEY_DISCARD ),
	FUSE_OPT_END
};

static const struct fuse_opt mfs_opts[] = {
	FUSE_OPT_KEY( "-t "	 	, (int)KEY_TRANCHE	),
	FUSE_OPT_KEY( "no_source_cache"	, (int)KEY_NO_SOURCE_CACHE ),

	FUSE_OPT_KEY( "-d"		, KEY_DEBUG		),
	FUSE_OPT_KEY( "debug"		, KEY_DEBUG		),
	FUSE_OPT_KEY( "-f"		, KEY_FOREGROUND	),
	FUSE_OPT_KEY( "-o "		, FUSE_OPT_KEY_KEEP	),
	FUSE_OPT_KEY( "-s"		, FUSE_OPT_KEY_KEEP	),
	FUSE_OPT_END
};

static const char units[]="bBkKmMgGtT";
static const unsigned long multiple[]=
 { 
	1UL				, 1UL,
	1000UL				, 1024UL,
	1000UL*1000UL			, 1024UL*1024UL,
	1000UL*1000UL*1000UL 		, 1024UL*1024UL*1024UL,
	1000UL*1000UL*1000UL*1000UL	, 1024UL*1024UL*1024UL*1024UL
 };


static bool is_rw = false;

struct chunk {
  int    fd;
  off_t  cfrom;
  size_t csize;
	/*
	 During the initialisation phase we need the actual file size of chunks.
	 Once we have finished reading args, this acutal size is not needed
	 but we need the global offset of this chunk file into the merged result.
	 So we use fsize during initialisation, then we use gfrom once the
	 mount is live. main() computes the gfrom-s once initialisation succeeds.
	*/

  union {
	size_t fsize;
	off_t  gfrom;
        } w;
};

static struct map {
 bool	  	mountdir_is_set;
 struct stat	mount_st;
 char const  	*filename;
 struct stat 	st;
 int    	chunkc;
 unsigned int	nlogcrit;
 unsigned int	nlogerr;
 bool		no_source_cache;
 bool		is_foreground;
 bool		is_debug;
 struct chunk	*chunkv;
} map = { false, {0}, NULL, {0}, 0, 0, 0, false, false, false, NULL };

static struct chunk *pendchunks;


int mfs_clean(int ret)
{
	int i;

	if ( NULL != map.chunkv ) {
		for (i=1; i<map.chunkc; i++) close(map.chunkv[i].fd);
		free(map.chunkv);
	}
	return ret;
}

void mfs_error(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	exit(mfs_clean(EXIT_FAILURE));
}

void mfs_log(int level, const char *msg)
{
	if (map.is_foreground) {
		fprintf(stderr,"[mfs] %s\n", msg);
	} else {
		syslog( level | LOG_DAEMON, "[mfs] %s", msg);
	}
}

static int mfs_getattr(const char *path, struct stat *stbuf)
{
	int res = 0;

	memset(stbuf, 0, sizeof(struct stat));
	if (strcmp(path, "/") == 0) {
		memcpy(stbuf, &map.mount_st, sizeof(struct stat));
	} else if (strcmp(path + 1, map.filename) == 0) {
		memcpy(stbuf, &map.st, sizeof(struct stat));
	} else
		res = -ENOENT;

	return res;
}

static int mfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			off_t offset, struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;

	if (strcmp(path, "/") != 0)
		return -ENOENT;

	return	( 0 == filler(buf, ".", &map.mount_st, 0)  &&
		  0 == filler(buf, "..", NULL, 0) &&
		  0 == filler(buf, map.filename, &map.st, 0) ) ? 0 : 1 ;
}

static int mfs_open(const char *path, struct fuse_file_info *fi)
{
	if (strcmp(path + 1, map.filename) != 0)
		return -ENOENT;

	if (!is_rw && (fi->flags & 3) != O_RDONLY)
		return -EACCES;

	return 0;
}

int mfs_comp( const void *pkey, const void *pelt)
{
	off_t offset;
	struct chunk *pchunk;

	offset= *(off_t *)pkey;
	pchunk= (struct chunk *)pelt;

	if ( offset >= pchunk->w.gfrom )
		if ( offset < pchunk->w.gfrom + pchunk->csize )
			return 0;
		else
			return 1;
	else
		return -1;
}

static int mfs_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	int err;
	size_t copied=0, sz;
	unsigned int nlog;
	char errbuf[80];
	struct chunk *pchunk;

	(void) fi;

	if (strcmp(path +1, map.filename) != 0)
		return -ENOENT;

	/*
	 We do a binary search to find the chunk corresponding to our offset,
	 which will be the first chunk file from which we will start reading.
	*/
	pchunk= bsearch( &offset, map.chunkv, map.chunkc, sizeof(struct chunk), mfs_comp );
	if (pchunk == NULL)
		return 0;

	while ( copied < size && pchunk < pendchunks ) {
		sz= (size - copied + offset > pchunk->csize + pchunk->w.gfrom  ) ? pchunk->csize + pchunk->w.gfrom - offset : size - copied;
		if ( sz != (size_t)pread( pchunk->fd, buf + copied, sz, offset - pchunk->w.gfrom + pchunk->cfrom ) ) {
		/*
		 This could only happen when the underlying file was altered or there was an I/O error, so this is a critical error.
		*/
			err= (-1 == sz) ? errno : EINVAL;
			if (map.nlogcrit < MAX_LOG) {
				nlog= __sync_fetch_and_add( &map.nlogcrit, 1 );
				if (nlog < MAX_LOG) {
					mfs_log(LOG_CRIT, "Critical error: I/O error or unexpected end of file.");
					snprintf( errbuf, sizeof(errbuf), "Reading %jd:%zu from %zu bytes at offset %jd in file %u\n", (intmax_t)offset, size, sz, (intmax_t)offset - pchunk->w.gfrom + pchunk->cfrom, (unsigned int)(pchunk - map.chunkv) ); 
					mfs_log(LOG_CRIT, errbuf );
					if ( EINVAL == err )
						mfs_log(LOG_CRIT, "Possibly, one of the merged files has been altered by another program.");
					else
						mfs_log(LOG_CRIT, strerror_r(err, errbuf, sizeof(errbuf)) );
				} else if (nlog == MAX_LOG) 
					mfs_log(LOG_CRIT, "Stop logging critical errors, threshold reached.");
			}
			return -err;
		}
		if (map.is_debug) fprintf(stderr, "[mfs] read %zu bytes at offset %jd from file %u\n", sz, (intmax_t)offset - pchunk->w.gfrom + pchunk->cfrom, (unsigned int)(pchunk - map.chunkv) );
		copied += sz;
		offset += sz;
		if ( map.no_source_cache) {
			if ( 0 != posix_fadvise(pchunk->fd, 0, 0, POSIX_FADV_DONTNEED) ) {
				if (map.nlogerr < MAX_LOG) {
					nlog= __sync_fetch_and_add( &map.nlogerr, 1 );
					if (nlog < MAX_LOG) mfs_log(LOG_ERR, "Error advising the kernel not to cache source files.");
					else if (nlog == MAX_LOG) {
						mfs_log(LOG_ERR, "Stop advising kernel not to cache source files!");
						map.no_source_cache= false;
					}
				}
			}
		}
		pchunk++;
	}

	return (int)copied;
}

static int mfs_write(const char *path, const char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	int err;
	size_t copied=0, sz;
	unsigned int nlog;
	char errbuf[80];
	struct chunk *pchunk;

	(void) fi;

	if (strcmp(path +1, map.filename) != 0)
		return -ENOENT;

	/*
	 We do a binary search to find the chunk corresponding to our offset,
	 which will be the first chunk file from which we will start writing.
	*/
	pchunk= bsearch( &offset, map.chunkv, map.chunkc, sizeof(struct chunk), mfs_comp );
	if (pchunk == NULL)
		return 0;

	while ( copied < size && pchunk < pendchunks ) {
		sz= (size - copied + offset > pchunk->csize + pchunk->w.gfrom  ) ? pchunk->csize + pchunk->w.gfrom - offset : size - copied;
		if ( sz != (size_t)pwrite( pchunk->fd, buf + copied, sz, offset - pchunk->w.gfrom + pchunk->cfrom ) ) {
			err= errno;
			if (map.nlogcrit < MAX_LOG) {
				nlog= __sync_fetch_and_add( &map.nlogcrit, 1 );
				if (nlog < MAX_LOG) {
					mfs_log(LOG_CRIT, "Critical error: I/O error.");
					snprintf( errbuf, sizeof(errbuf), "Writing %jd:%zu from %zu bytes at offset %jd in file %u\n", (intmax_t)offset, size, sz, (intmax_t)offset - pchunk->w.gfrom + pchunk->cfrom, (unsigned int)(pchunk - map.chunkv) ); 
					mfs_log(LOG_CRIT, errbuf );
				} else if (nlog == MAX_LOG) 
					mfs_log(LOG_CRIT, "Stop logging critical errors, threshold reached.");
			}
			return -err;
		}
		if (map.is_debug) fprintf(stderr, "[mfs] write %zu bytes at offset %jd from file %u\n", sz, (intmax_t)offset - pchunk->w.gfrom + pchunk->cfrom, (unsigned int)(pchunk - map.chunkv) );
		copied += sz;
		offset += sz;
		pchunk++;
	}

	return (int)copied;
}

static struct fuse_operations mfs_oper = {
	.getattr	= mfs_getattr,
	.readdir	= mfs_readdir,
	.open		= mfs_open,
	.read		= mfs_read,
	.write		= mfs_write,
};


static int mfs_opt_count(void *data, const char *arg, int key, struct fuse_args *outargs)
{
	struct map *pmap;
	pmap=(struct map *)data;

	(void)arg;

	switch (key) {
		case FUSE_OPT_KEY_NONOPT :
			pmap->chunkc++;
			break;


		case KEY_HELP :
  			fprintf(stderr,
				"Usage: mfs mountpoint  file [-t start[unit]:size[unit]] \\\n"
				"                       file [-t start[unit]:size[unit]] \\\n"
				"                      [file [-t start[unit]:size[unit]]]...\n"
				"\n"
				"    You must specify at least 2 regular files (merging 1 file is useless!)\n"
				"\n"
				"    When mount is successful, a single file is shown on the mountpoint.\n"
				"    This file has the name and attributes of the first file on the list,\n"
				"        except for write permissions (zeroed) and size (set to the total\n"
				"        size of the merge).\n"
				"\n"
				"\n"
				"General options:\n"
				"    -o opt,[opt...]  mount options\n"
				"    -h   --help      print help\n"
				"    -V   --version   print version\n"
				"\n"
				"\n"
				"mfs options:\n"
				"    -o rw  will allow writing, otherwise, by default it's safely read-only.\n"
				"    -o no_source_cache\n"
				"\n"
				"    The default is to cache source files only and not the merged result.\n"
				"    When using kernel_cache or auto_cache, both source and result are cached,\n"
				"    which can be a poor use of kernel cache. This option will advise the kernel\n"
				"    that caching the source files is not needed, reducing the cache footprint.\n"
				"\n"
				"\n"
				"    -t start[unit]:size[unit]\n"
				"\n"
				"    The -t option restricts the preceding file to the specified tranche.\n"
				"    If several tranches are specified for the same file the last one is used.\n"
				"\n"
				"    start and size must be integer numbers, optionnally followed by a unit:\n"
				"        B, K, M, G or T: for Byte, KiB, MiB, GiB, TiB (in 1024 scale)\n"
				"      or\n"
				"        b, k, m, g or t: for Byte, KB, MB, GB, TB (in 1000 scale)\n"
				"\n"
				"    When start or size are negative, they are relative to the file size.\n"
				"\n"
				"\n"
				"Default values:\n"
				"        start: 0\n"
				"        size : the number of byte from start to the end of file.\n"
				"        units: b\n"
				"\n"
				"\n"
				"Examples:\n" 
				"        -t -10m:2m    = tranche of 2MB starting at 10MB from the end of file\n"
				"        -t :-1G       = all the file but the last GiB\n"
				"        -t 1024:4096  = 4096 bytes starting at offset 1024\n"
				"        -t :	       = valid but useless since it means the whole file!\n"
				"\n"
				"\n"
				);
			if ( 0 != fuse_opt_add_arg(outargs, "-ho") )
				mfs_error("Error adding option argument (fuse_opt_add_arg).\n");
			exit( (0 == fuse_main(outargs->argc, outargs->argv, &mfs_oper, NULL)) ? EXIT_SUCCESS : EXIT_FAILURE );


		case KEY_VERSION :
			fprintf(stderr, MFS_VERSION);
			if ( 0 != fuse_opt_add_arg(outargs, "--version") )
				mfs_error("Error adding option argument (fuse_opt_add_arg).\n");
			exit( (0 == fuse_main(outargs->argc, outargs->argv, &mfs_oper, NULL)) ? EXIT_SUCCESS : EXIT_FAILURE );


		case KEY_RW:
			fprintf(stderr,"ATTENTION: have a copy of the files in case something goes wrong!\n");
			is_rw = true;
	}
	return 0;
}

static int mfs_opt_proc(void *data, const char *arg, int key, struct fuse_args *outargs)
{
	struct map *pmap;
	struct stat st;
	off_t  from, size;
	char fromunit, sizeunit, extra;
	char *s;

	(void)outargs;

	pmap=(struct map *)data;

	switch (key) {
		case FUSE_OPT_KEY_NONOPT :

			if ( ! pmap->mountdir_is_set ) {
				if ( -1 == stat(arg, &pmap->mount_st) ) {
				        perror(NULL);
					mfs_error("Error getting stats of `%s`\n", arg);
			        }
				if ( ! S_ISDIR(pmap->mount_st.st_mode) )
					mfs_error("`%s` is not a directory\n", arg);
				if (!is_rw)
					pmap->mount_st.st_mode &= ~( S_IWUSR | S_IWGRP | S_IWOTH );
				pmap->mount_st.st_nlink=2;
				pmap->mountdir_is_set= true;
				return 1;
			} else {
			        pmap->chunkv[pmap->chunkc].fd= open(arg, (is_rw) ? O_RDWR : O_RDONLY);
			        if ( -1 == pmap->chunkv[pmap->chunkc].fd ) {
				        perror(NULL);
					mfs_error("Error opening `%s`\n", arg);
			        }
				if ( -1 == fstat(pmap->chunkv[pmap->chunkc].fd, &st) ) {
				        perror(NULL);
					mfs_error("Error getting stats of `%s`\n", arg);
			        }
				if ( S_ISBLK(st.st_mode) ) {
				/*
				 Note that for a block device all size related fields in the
				 stat structure are empty. We have to use iotcl to get the size
				 and store in the stat structure we use for further operations.
				*/
					ioctl(pmap->chunkv[pmap->chunkc].fd, BLKGETSIZE64, &st.st_size);
				}
				else {
					if ( ! S_ISREG(st.st_mode) )
						mfs_error("`%s` is not a regular or block file\n", arg);
				}
				if ( 0 == st.st_size ) {
					fprintf(stderr, "WARNING: `%s` is empty, skipping this file\n", arg);
				        return 0;
				}
				if ( NULL == pmap->filename ) {
				/*
				 Filename that we will show. Copying the stat
				 structure of that file, putting Write permission to 0.
				 Size is computed later in the main when all the files
				 have been 'stat'-ed.
				 Note we make the file a regular file even if first argument was
				 a block device.
				*/
					memcpy( &pmap->st, &st, sizeof(struct stat) );
					s= strrchr(arg, '/');
					pmap->filename = (NULL == s) ? arg : s+1;
					if (is_rw)
						pmap->st.st_mode &= ~( S_IFMT );
					else
						pmap->st.st_mode &= ~( S_IFMT | S_IWUSR | S_IWGRP | S_IWOTH );
					pmap->st.st_mode |= S_IFREG;
					pmap->st.st_nlink=1;
				}
				pmap->chunkv[pmap->chunkc].cfrom  = 0;
				pmap->chunkv[pmap->chunkc].csize  = (size_t)st.st_size;
				pmap->chunkv[pmap->chunkc].w.fsize= st.st_size;
				pmap->chunkc++;
				return 0;
			}



		case KEY_TRANCHE :
		
			if ( 0 == pmap->chunkc )
				mfs_error("Invalid tranche `%s`: a tranche must follow a chunk file argument\n", arg);
			s = strchr(arg,':');
			if ( NULL == s )
				mfs_error("Invalid tranche: no ':' found in `%s`\n", arg);
			else
				*s='\0';
			extra='\0';
			fromunit='b';
			if ( '\0' != *(arg + 2) ) {
				if ( 0 == sscanf(arg + 2, "%jd%c%c", (intmax_t *)&from, &fromunit, &extra) ) {
					*s=':';
					mfs_error("Invalid tranche: no integer for 'start' in `%s`\n", arg);
				}
			} else	from= 0;
			*s=':';
			sizeunit='b';
			if ( '\0' != *(s + 1) ) {
				if ( 0 == sscanf(s + 1  , "%jd%c%c", (intmax_t *)&size, &sizeunit, &extra) )
					mfs_error("Invalid tranche: no integer for 'size' `%s`\n", arg);
				if ( 0 == size )
					mfs_error("Invalid zero size tranche `%s`\n", arg);
			} else size= 0;
			if ( '\0' != extra || NULL == strchr(units,fromunit) || NULL == strchr(units,sizeunit) )
				mfs_error("Invalid tranche: invalid units or extraneous characters in: `%s`\n", arg);
			from *= multiple[ (strchr(units,fromunit) - units) ];
			size *= multiple[ (strchr(units,sizeunit) - units) ];
			if ( from < 0 )  from += pmap->chunkv[pmap->chunkc - 1].w.fsize;
			if ( size < 0 )  size += pmap->chunkv[pmap->chunkc - 1].w.fsize;
			if ( 0 == size && from < (off_t)pmap->chunkv[pmap->chunkc - 1].w.fsize) size= pmap->chunkv[pmap->chunkc - 1].w.fsize - from;
			if ( from < 0 || size < 0 || size + from > pmap->chunkv[pmap->chunkc - 1].w.fsize )
				mfs_error("Invalid tranche `%s`: incoherent with chunk file size %zu\n", arg, pmap->chunkv[pmap->chunkc - 1].w.fsize);
			pmap->chunkv[pmap->chunkc - 1].cfrom= (off_t)from;
			pmap->chunkv[pmap->chunkc - 1].csize= (size_t)size;

			return 0;


		case KEY_NO_SOURCE_CACHE:
			pmap->no_source_cache = true;
			return 0;

		case KEY_DEBUG:
			pmap->is_debug = true;
			/* no 'break' since debug implies foreground */

		case KEY_FOREGROUND:
			pmap->is_foreground = true;
			break;
	}
	return 1;
}


int main(int argc, char *argv[])
{
	int i;

	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

	/*
	 Count the non-option arguments to allocate enough memory.
	 We must have at least 3: mountpoint + 2 files.
	 We also take care here of help and version flags, so that
	 we exit as soon as possible when they are present.
	 */

	if ( 0 != fuse_opt_parse(&args, &map, mfs_count, mfs_opt_count) )
		mfs_error("Error parsing arguments.\n" );

	fuse_opt_free_args(&args);
	if ( 3 > map.chunkc ) 
		mfs_error("There must be at least 2 files for the merge to be relevant: not mounting.\n" );

	/*
	 Now we read/check all the arguments in the allocated memory.
	 We might still over-allocate in case there are empty files.
	*/
	map.chunkv= malloc( (map.chunkc - 1) * sizeof(struct chunk));
	if ( NULL == map.chunkv )
		mfs_error("Error allocating memory for chunks files");
	map.chunkc= 0;
	args.argc= argc;
	args.argv= argv;
	if ( 0 != fuse_opt_parse(&args, &map, mfs_opts, mfs_opt_proc) )
		mfs_error("Error parsing arguments.\n" );
	if ( 2 > map.chunkc ) 
		mfs_error("There must be at least 2 non-empty files to merge: not mounting.\n" );

	/*
	 Computing total size and offsets of our "merge".
	 */
	map.chunkv[0].w.gfrom= 0;
	map.st.st_size = map.chunkv[0].csize;
	for (i=1; i<map.chunkc; i++) {
		map.st.st_size += map.chunkv[i].csize;
		map.chunkv[i].w.gfrom = map.chunkv[i-1].w.gfrom + map.chunkv[i-1].csize;
	}
	pendchunks= map.chunkv + map.chunkc;


	/*
	 It is cleaner to force the mount to read_only than to get
	 messages like "function not implemented" when trying to write,
	 touch, chmod, chown or delete a file in the mount directory.
	 With the read-only option forced, those 'write' accesses
	 will be handled by the kernel and won't even reach our code.
	 */
	fuse_opt_add_arg(&args, (is_rw)? "-onoatime" : "-oro,noatime");

	/*
	 Starting the fuse mount!
	 */
	return mfs_clean( fuse_main(args.argc, args.argv, &mfs_oper, NULL) );

}
